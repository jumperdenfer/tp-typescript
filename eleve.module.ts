export class Eleve{
  public _prenom : string;
  public _age : number;
  public _adresse : Adresse;
  public _note : Array<number>;
  public _languages : Array<string>;
  constructor(data){
    this.prenom = data.prenom;
    this.age = data.age;
    this.note = data.note;
    this.adresse = data.adresse;
  }
  //Get and Set for prenom
  get prenom(){
    return this._prenom
  }
  set prenom(prenom : string){
     this._prenom  = prenom;
  }
  //Get and Set for age
  get age(){
    return this._age;
  }
  set age(age : number){
    this._age = age;
  }
  //Get and Set for adresse
  get adresse(){
    return this._adresse;
  }
  set adresse(adresse : Adresse){
    this._adresse = adresse;
  }
  //Get and Set for languages
  get languages(){
    return this._languages;
  }
  set languages(languages : Array<string>){
    this._languages = languages;
  }
  //Get and Set for note
  get note(){
    return this._note;
  }
  set note(note: Array<number>){
    let allNote = [];
    for(let element of note){
      if(element >= 0 && element <= 20) {
        allNote.push(element);
      }
    }
    this._note = allNote;
  }

  //Get prenom,nom & age
  getInfo() : Array<string>{
    let info = []
    info['prenom'] = this.prenom;
    info['age'] = this.age;
    info['adresse'] = this.adresse;
    return info;
  }

  //Get moyenne
  getMoyenne(){
    let note = this.note;
    let value : number = 0;
    let divided :number = this.note.length;
    for(let elements of note){
      value += elements
    }
    let result : number = value / divided
    return result
  }
  getNote(){
    let moyenne = this.getMoyenne();
    let note = this.note;
    note.push(moyenne);
    return note;
  }
}
export interface Adresse {
  rue : string;
  cp : number|string;
  ville: string;
  appartement? : string;
}
