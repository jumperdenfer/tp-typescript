import {Eleve} from './eleve.module';
import {Adresse} from './eleve.module';
console.log('ok');

//Créé des varialbe vide qui vont contenir uniquement des chiffre
let nombre_a : number = 1;
let nombre_b : number = 2;

//Ajoute une valeur à la variable
nombre_a = 5;

//nombre_b = 'a'; Renvoi une erreur

//Définie une variable vide qui peut contenir unquement des charactères
let prenom : string = 'benoit';
let age : number = 24;

/*
* Fonction hello
*/
function hello(prenom:string,age:number) : string{
  return `Bonjour ${prenom}, tu as ${age}`
}

let rep =  hello("benoit",24);


let tableau : Array<string>  = ['benoit','kevin','jonathan','alexis'];
function hello_friends(tableau : Array<string>) : void {
  for(let prenom in tableau){
      let salut = `Salut ${tableau[prenom]} ! `;
      console.log(salut);
  }
}
hello_friends(tableau);

let monAdresse = {
  'rue' : 'Rue du pouvoir',
  'cp' : '15150',
  'ville' : 'Paris'
}
function adresse_view(adress : {'rue' : string, 'cp' : string|number, 'ville' : string, 'appartement'? : string }) : void {
  console.log(`Mon adresse est ${adress.rue}, ${adress.cp}, ${adress.ville}`)
}
adresse_view(monAdresse)

let test2 : object = {
  'rue' : 'test',
  'cp' : "31150",
  'ville' : 'testdeville'
}
function testAdressVille(params : object){
  console.log(`adresse test : ${params['rue']} ; ${params['cp']} ; ${params['ville']}`)
}
testAdressVille(test2)
//Définition de la class Eleve

let eleve1Data = {
  "prenom" : 'jean',
  'age' : 20,
  'adresse' : ["des bouffardière","38160","Saint-Romans"],
  'note' : [18,15,14,14,19,-5,99]
}
let eleve1 = new Eleve(eleve1Data);
console.log(eleve1.getInfo());
console.log(eleve1.getMoyenne());
console.log(eleve1.getNote());



namespace NL_Lang {
  export class Languages{
    public _list : Array<string>;

    constructor(){
      this.list = ['PHP','JavaScript','HTML','CSS','Ruby'];
    }
    //Getter et Setter pour list
    get list(){
      return this._list;
    }
    set list(params : Array<string>){
      this._list = params
    }
  }
}
let langue = new NL_Lang.Languages()
eleve1.languages = langue.list;
console.log(eleve1);
